#include "stdio.h"
#include "libc.h"
#include <curses.h>
#include <time.h>

#define BOARD_COLS 10
#define BOARD_ROWS 18

#define NUM_PIECES 7

#define EMPTY_BLOCK 0
#define FILLED_BLOCK 1

#define LEFT_MARGIN ((TERM_COLS - BOARD_COLS) / 2)
#define TOP_MARGIN ((TERM_ROWS - BOARD_ROWS) / 2)

int TERM_COLS;
int TERM_ROWS;

char board[BOARD_COLS][BOARD_ROWS];

char currentPiece[5][5];
int currentPieceX;
int currentPieceY;

int gameOver;


char pieces[][5][5] = {
	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0},
	},

	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
	},
	
	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 1, 1, 0, 0},
		{0, 0, 0, 0, 0},
	},

	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0},
	},

	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 1, 1, 0, 0},
		{0, 1, 0, 0, 0},
		{0, 0, 0, 0, 0},
	},

	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0},
	},
	
	{
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
	},

};

void drawWalls() {
	for (int board_row = 0; board_row < BOARD_ROWS; board_row++) {
		move(TOP_MARGIN + board_row, LEFT_MARGIN - 1);
		addstr("|");
	}
	
	for (int board_row = 0; board_row < BOARD_ROWS; board_row++) {
		move(TOP_MARGIN + board_row, LEFT_MARGIN + BOARD_COLS);
		addstr("|");
	}
	
	for (int board_col = -1; board_col < BOARD_COLS + 1; board_col++) {
		move(TOP_MARGIN + BOARD_ROWS, LEFT_MARGIN + board_col);
		addstr("-");
	}
}

void checkForGameOver() {
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			if (currentPiece[col][row] == FILLED_BLOCK && board[currentPieceX + col][currentPieceY + row] == FILLED_BLOCK)
				gameOver = TRUE;
}

void addNewRandomPiece() {
	int newPiece;
	
	newPiece = random() % NUM_PIECES;
	
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			currentPiece[col][row] = pieces[newPiece][col][row];
	
	currentPieceX = BOARD_COLS / 2 - 2;
	currentPieceY = -1;
	
	checkForGameOver();
}

void rotateCurrentPiece() {
	char newPiece[5][5];
	
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			newPiece[col][row] = currentPiece[row][5 - col];
			
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			currentPiece[col][row] = newPiece[col][row];
}

int checkHorizontalCollision() {
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			if (currentPiece[row][col] == FILLED_BLOCK) {
				if (currentPieceX + col < 0 || currentPieceX + col > BOARD_COLS - 1)
					return TRUE;
				else if (board[currentPieceX + col][currentPieceY + row] == FILLED_BLOCK)
					return TRUE;
			}
			
	return FALSE;
}

int checkVerticalCollision() {
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			if (currentPiece[row][col] == FILLED_BLOCK) {
				if (currentPieceY + row > BOARD_ROWS - 1)
					return TRUE;
				else if (board[currentPieceX + col][currentPieceY + row] == FILLED_BLOCK)
					return TRUE;
			}
			
	return FALSE;
}

void checkAndRemoveFullLines() {
	int lineIsFull;
	
	for (int row = 0; row < BOARD_ROWS; row++) {
		lineIsFull = TRUE;

		for (int col = 0; col < BOARD_COLS; col++)
			lineIsFull &= (board[col][row] == FILLED_BLOCK);
			
		if (lineIsFull)	{
			for (int row = BOARD_ROWS - 1; row > 0; row--)
				for (int col = 0; col < BOARD_COLS; col++)
					board[col][row] = board[col][row - 1];
					
			row--;
		}
	}
}

void mergePiece() {
	for (int col = 0; col < 5; col++)
		for (int row = 0; row < 5; row++)
			if (currentPiece[row][col])
				board[currentPieceX + col][currentPieceY + row - 1] = currentPiece[row][col];
				
	checkAndRemoveFullLines();
}

void scrollDown() {
	currentPieceY++;
		
	if (checkVerticalCollision() == TRUE) {
		mergePiece();
		addNewRandomPiece();
	}
}

void drawBoard() {
	for (int board_row = 0; board_row < BOARD_ROWS; board_row++) {
		for (int board_col = 0; board_col < BOARD_COLS; board_col++) {
			move(TOP_MARGIN + board_row, LEFT_MARGIN + board_col);
			
			if (board[board_col][board_row] == FILLED_BLOCK)
				addstr("#");
		}
	}
}

void drawCurrentPiece() {
	for (int row = 0; row < 5; row++)
		for (int col = 0; col < 5; col++)
			if (currentPiece[row][col] == FILLED_BLOCK) {
				move(TOP_MARGIN + currentPieceY + row, LEFT_MARGIN + currentPieceX + col);
				
				addstr("#");
			}
}

void initNewGame() {
	// Initialize the board
	for (int col = 0; col < BOARD_COLS; col++)
		for (int row = 0; row < BOARD_ROWS; row++)
			board[col][row] = EMPTY_BLOCK;
	
	gameOver = FALSE;

	addNewRandomPiece();
}

void rotateIfPossible() {
	// Handling piece rotation
	// Let's backup the current piece then rotate and rollback if any collision.
	char backupPiece[5][5];
	
	for (int row = 0; row < 5; row++)
		for (int col = 0; col < 5; col++)
			backupPiece[col][row] = currentPiece[col][row];
		
	rotateCurrentPiece();
	
	if (checkVerticalCollision() == TRUE || checkHorizontalCollision() == TRUE)
		for (int row = 0; row < 5; row++)
			for (int col = 0; col < 5; col++)
				currentPiece[col][row] = backupPiece[col][row];
}

int main(int argc, char **argv) {

	// Initialize Curses
	initscr();

	// Disable cursor (ANSI escape code)
	printf("\x1B[?25l"); 

	// Set Curses in no-delay mode to handle key presses
	nodelay(stdscr, TRUE);
	
	// Enable keypad to catch arrow keys
	keypad(stdscr,TRUE);

	

	srandom(time(NULL));

	initNewGame();	
	
	int frame_counter = 0;
	
	int last_key;
	
	while ((last_key = getch()) != 27 && last_key != 'q') { //key code 27 is escape key, no constant in Curses for this one!
		
		int lastPieceX = currentPieceX;
		
		if (gameOver == FALSE || last_key == 'n')
			switch (last_key) {
				case 'n':
					if (gameOver)
						initNewGame();
					break;
				case KEY_LEFT:
					currentPieceX--;
					break;
				case KEY_RIGHT:
					currentPieceX++;
					break;
				case KEY_UP:
				case ' ':
					rotateIfPossible();
					break;
				case KEY_DOWN:
					scrollDown();
					break;
			}

		if (checkHorizontalCollision() == TRUE)
			currentPieceX = lastPieceX;
	
		clear();
		
		drawWalls();
		
		// Getting terminal size	
		struct ttysize ts; 
		ioctl(0, TIOCGWINSZ, &ts);
		TERM_COLS = ts.ts_cols;
		TERM_ROWS = ts.ts_lines;
		
		drawBoard();
		
		drawCurrentPiece();
		
		if (gameOver && frame_counter % 60 < 30) {
			move(TERM_ROWS / 2 - 1, TERM_COLS / 2 - 5);
			addstr("GAME OVER!");
			move(TERM_ROWS / 2 + 1, TERM_COLS / 2 - 11);
			addstr("Press N to play again.");
		}
		
		// Flush Curses
		refresh();
		
		usleep(1.0 / 60.0 * 1000000.0);
		
		frame_counter++;
		
		if (gameOver == FALSE && frame_counter % 60 == 59)
			scrollDown();
	}
	
	// Enable cursor (ANSI escape code)
	printf("\x1B[?25h");
	
	// Restore terminal after Curses
	endwin();
}

